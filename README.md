# pumpbridge

connects pump.io to twitter, facebook and googleplus

THIS SOFTWARE IS A PROOF OF CONCEPT DEMONSTRATION

## Features

###g+ (read only)

* get all public posts from your circles
  and convert it to pump notes
  (text, links, pics, videos)

###facebook (read / write)

* get facebook newsfeed including some non-public posts
  (text, links, pics, videos)
  depending on your friends privacy configuration!
  For example for those who have disabled all Platform apps 
  from accessing their data
  http://stackoverflow.com/questions/11135053/

* post public pump posts and public shares to facebook

* likes and comments to facebook related pump notes
  are synced to facebook

###twitter (read / write)

* get all tweets and retweets of the people you're following on twitter

* post public pump posts and public shares to twitter

###benefits

* share posts between networks.
  For example, you can share a post from googleplus/facebook/twitter to your followers in pump.io.

##start bridging

* login to a remote pump.io account to get started

* check your pump.io id (logged in as:)

* login to your established social network:

    * Twitter:
        * click on Twitter icon
        * login
        * your account will be synced immediately
          and you got all posts within the last interval (default 15min)

    * Facebook:
        * click on Facebook icon
        * login
        * click save credentials
        * your account will be synced to the next interval (default 15min)
        * pumpbridge syncs your public pump.io posts you made within the last interval
        * pumpbridge syncs facebooks posts within the last interval
          (depending on timezone it differs a little bit)

    * Googleplus:
        * click on Google icon
        * login
        * click save credentials
        * your account will be synced to the next interval (default 15min)
        * pumpbridge syncs google posts within the last interval
          (since g+-api is read only you can not post to googleplus)

* click save credentials

* logout

##stop bridging

* login to a remote pump.io account

* click on delete credentials on the specify social network
  (in certain circumstances you still see your loginname even
  if all token were deleted)

#Video

https://www.youtube.com/watch?v=SB2CGFnrnLY (older version without )

#SSL

see https://github.com/e14n/pump.io/wiki/StartSSL-Free-Certificate

#Bugs

#To-Do and wishlist

* sync comments from googleplus to pump.io
* sync comments from facebook to pump.io
* sync comments from pump.io to facebook

# License

Copyright (C) 2014
Intevation GmbH <intevation@intevation.de>
Neuer Graben 17, D-49074 Osnabrueck,
Germany

Parts from https://github.com/e14n/pump2status
Copyright 2013, E14N https://e14n.com/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

##Authors

Mathias Gebbe <mgebbe@intevation.de>

##Media

* bridge (bridge.png): http://commons.wikimedia.org/wiki/File:Bridge-stone.svg
* facebook (fb.svg): http://commons.wikimedia.org/wiki/File:F_icon.svg
* googleplus (gp.svg): http://commons.wikimedia.org/wiki/File:Google_plus.svg
* pump.io (pumpiologo.svg): https://en.wikipedia.org/wiki/Pump.io#mediaviewer/File:Pump.io.svg
* background (grey_wash_wall.png): http://subtlepatterns.com/grey-washed-wall/ (CC BY-SA 3.0)

##Sources

The source files can be check out as follows
```
hg clone https://hg.wald.intevation.org/pumpbridge/
```
this software is based on https://github.com/e14n/pump2status and
uses many tools made by https://github.com/e14n/
