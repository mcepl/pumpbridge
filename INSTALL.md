# installation

### what you need

* facebook api key: https://developers.facebook.com/
* googleplus api key and secret: https://console.developers.google.com/
* twitter api key and secret: https://dev.twitter.com/
* bridge pump.io user (log in and dont map it to social networks)
  for example: bridge@io.intevation.de (https://io.intevation.de/bridge)

### how to install

```
npm install
```

* copy app.js and index.js to pump.io-client-app usually with:

```
cp app.js_to_pump.io-client-app_lib node_modules/pump.io-client-app/lib/app.js
cp index.js_to_pump.io-client-app_lib_routes node_modules/pump.io-client-app/lib/routes/index.js
mkdir -p lib/ && coffee -b -c -o lib/ src/*.coffee
cp src/restartSYNC.sh lib/restartSYNC.sh
```

* configure your Facebook App ID and your Google App ID in
```
views/index.jade
 appId: 'FACEBOOKID',
 'cookiepolicy': "https://yourserver.org",
 'clientid': 'GOOOGLEPLUSCLIENTID',
```

* copy and configure pumpbridge.json.example to /etc/pumpbridge.json

```
cp pumpbridge.json.example /etc/pumpbridge.json
```
```
{
"driver": "redis",
"params": {"host":"localhost","database":1,"port":6379},
"secret": "takeanother",
"port": 443,
"bounce": true,
"interval": 900000,
"name": "pumpbridge",
"description": "connect social networks",
"urlPort": 443,
"hostname": "server.com",
"address": "42.42.42.42",
"nologger": false,
"serverUser": "pumpio",
"debugClient": true,
"bridgeid": "bridge@io.intevation.de",
"gpAPI_KEY": "XXXXXXXX-GOOGLEAPIKEY",
"gpCLIENTID": "THEGOOGLECLIENDID",
"gpCLIENTSECRET": "THEGOOGLECLIENTSECRET",
"fbAPPID": "FACEBOOKAPPID",
"fbSECRET": "FACEBOOKSECRET",
"twclient_id": "TWITTERAPIKEY",
"twclient_secret": "TWITTERSECRET",
"key": "/etc/ssl/server.key",
"cert": "/etc/ssl/server.pem"
}
```
