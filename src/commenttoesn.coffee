# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

_ = require("underscore")
async = require("async")
DatabankObject = require("databank").DatabankObject
CommentToESN = DatabankObject.subClass("CommentToESN")

CommentToESN.schema =
  pkey: "uid"
  fields: [
    "ESNPost"
    "pumpComment"
    "created"
    "updated"
  ]
  indices: [
    "ESNPost"
    "pumpComment"
  ]

CommentToESN.beforeCreate = (props, callback) ->
  if not props.ESNPost or not props.pumpComment
    callback new Error("Need 'ESNPost' and 'pumpComment'"), null
    return
  props.uid = CommentToESN.key(props.pumpComment, props.ESNPost)
  props.created = Date.now()
  props.updated = Date.now()
  callback null, props
  return

CommentToESN::beforeSave = (callback) ->
  props = this
  if not props.ESNPost or not props.pumpComment
    callback new Error("Need 'ESNPost' and 'pumpComment'"), null
    return
  props.updated = Date.now()
  callback null
  return

CommentToESN.key = (pumpComment, ESNPost) ->
  pumpComment + "_to_" + ESNPost

module.exports = CommentToESN
