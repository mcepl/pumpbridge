# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

fs = require 'fs'
path = require 'path'
_ = require("underscore")

filename = '/etc/pumpbridge.json'

defaults =
  name: 'pumpbridge'
  description: 'bridge it'
  forwardInterval: 15 * 60 * 1000
  updateInterval: 12 * 60 * 60 * 1000
  params: {}
  views: path.join(__dirname, '..', 'views')
  static: path.join(__dirname, '..', 'public')
  foreign: 'pumpbridge'
  foreignName: null

if fs.existsSync(filename)
  config = _.defaults(JSON.parse(fs.readFileSync(filename)), defaults)
else
  config = defaults

exports.config = config
