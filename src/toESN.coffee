# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

_ = require("underscore")
async = require("async")
DatabankObject = require("databank").DatabankObject
ToESN = DatabankObject.subClass("toESN")

ToESN.schema =
  pkey: "uid"
  fields: [
    "sourceUser"
    "sourcePost"
    "targetUser"
    "targetPost"
    "recipientUser"
    "created"
    "updated"
  ]
  indices: [
    "uid"
    "sourceUser"
    "sourcePost"
    "targetUser"
    "targetPost"
  ]

ToESN.beforeCreate = (props, callback) ->
  if not props.sourcePost or not props.sourceUser
    callback new Error("Need 'sourceUser' and 'sourceUser'"), null
    return
  props.created = Date.now()
  props.updated = Date.now()
  callback null, props
  return

ToESN::beforeSave = (callback) ->
  props = this
  if not props.sourcePost or not props.sourceUser
    callback new Error("Need 'sourceUser' and 'sourceUser'"), null
    return
  props.updated = Date.now()
  callback null
  return

module.exports = ToESN
