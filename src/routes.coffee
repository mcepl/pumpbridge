# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

async = require("async")
_ = require("underscore")
PumpIOClientApp = require("pump.io-client-app")
databank = require 'databank'
Databank = databank.Databank
DatabankObject = databank.DatabankObject
RequestToken = PumpIOClientApp.RequestToken
userAuth = PumpIOClientApp.userAuth
userOptional = PumpIOClientApp.userOptional
userRequired = PumpIOClientApp.userRequired
noUser = PumpIOClientApp.noUser
Usermap = require("./usermap")
Google = require("./google")
Facebook = require("./facebook")
Twitterroutes = require("./twitterroutes")
Pump = require("./pumpio")
User = require("./user")
Edge = require("./edge")
Host = require("../node_modules/pump.io-client-app/lib/models/host")
https = require("https")
OAuth = require('oauth')
Sync = require('./sync')

addRoutes = (app, options) ->

  getBridge = (req, res) ->
    res.send 'Hello, i am the bridge you looking for!'
    return

  saveBridge = (req, res) ->

    pumpid = req.body.pumpid
    pumptoken = req.body.pumptoken
    pumpsecret = req.body.pumpsecret

    gpid = req.body.gpid
    gptoken = req.body.gptoken
    gpdelete = req.body.gpdelete

    fbid = req.body.fbid
    fbtoken = req.body.fbtoken
    fbdelete = req.body.fbdelete

    twdelete = req.body.twdelete
    
    twmode = req.body.twmode
    if(req.body.twtoesn?) then twtoesn=true else twtoesn=false
    if(req.body.twfromesn?) then twfromesn=true else twfromesn=false
    
    # create/update userdb entrys
    # SET THE CONNECTION BETWEENS PUMPIO AND ESN-ACCOUNT (facebook or google)

    #### PUMPIO STUFF ####
    if not (pumpid?)
      console.log 'no pumpio'
    else
      saveUsermap(pumpid,pumpid,pumptoken+';'+pumpsecret,pumpsecret, (err, result) ->
        Pump.getUser(result))

     #async.waterfall [(callback) ->
     #   searchUsermap(bridgeid,bridgeid, callback)
     #], (result) ->
     #  Pump.postUser(result, 'mgebbe@io.intevation.de','Hallo Welt')

    #### GOOGLE STUFF ####
    if not (gpid?) or not (gptoken?) or gpid is "" or gptoken is ""
      console.log 'no google'
    else
      async.waterfall [
        (callback) ->
          saveUsermap(pumpid,gpid,gptoken,'', callback)
        (user, callback) ->
          Google.getRefreshToken(user,callback)
        (token, callback) ->
          saveUsermap(pumpid,gpid,token.access_token + ";" + gptoken.substr(gptoken.indexOf(';')+1,gptoken.length),token.refresh_token,callback)
      ], (err, result) ->
        # now we have a gp refresh token
        #console.log 'done'
    if (gpdelete?)
      console.log "delete gp account"
      async.waterfall [
        (callback) ->
          Usermap.search {user_pumpio: gpdelete}, callback
        (result, callback) ->
          _.each result, (um) ->
            if um.user_ESN.indexOf('@google') isnt -1
              deleteUsermap(um.id,callback)
            return
       ], (err, result) ->


    #### FACEBOOK STUFF ####
    if not (fbid?) or not (fbtoken?) or fbid is "" or fbtoken is ""
      console.log 'no facebook'
    else
      # get long-lived token
      # (token is callback function)
      # get the long lived token from facebook
      Facebook.getLongLivedToken fbtoken,(token) ->
        if (token?)
          saveUsermap(pumpid,fbid,token,fbtoken, (err, result) ->
            console.log 'fbsave.')

    #### Twitter MODUS ###
    if (twmode?)
      console.log "mod tw account"
      async.waterfall [
        (callback) ->
          Usermap.search {user_pumpio: twmode}, callback
        (result, callback) ->
          _.each result, (um) ->
            if um.user_ESN.indexOf('@twitter') isnt -1
              saveUsermapSelect(um.user_pumpio, um.user_ESN, um.oauth_token, um.extra_token,twfromesn,twtoesn,callback)
            return
       ], (err, result) ->

    if (fbdelete?)
      console.log "delete fb account"
      async.waterfall [
        (callback) ->
          Usermap.search {user_pumpio: fbdelete}, callback
        (result, callback) ->
          _.each result, (um) ->
            if um.user_ESN.indexOf('@facebook') isnt -1
              deleteUsermap(um.id,callback)
            return
       ], (err, result) ->

    if (twdelete?)
      console.log "delete tw account"
      async.waterfall [
        (callback) ->
          Usermap.search {user_pumpio: twdelete}, callback
        (result, callback) ->
          _.each result, (um) ->
            if um.user_ESN.indexOf('@twitter') isnt -1
              deleteUsermap(um.id,callback)
            return
       ], (err, result) ->

    # go back to main page
    #res.send JSON.stringify(req.body.pumpobj)
    #res.send 'bridge update for:'+ pumpid + '<br>--> ' + gpid + ' ' + gptoken + '<br>--> ' + fbid + ' ' + fbtoken
    res.redirect('/')

    # END
    return

  # Routes
  app.get "/bridge", userAuth, userRequired, getBridge
  app.post "/bridge", userAuth, userRequired, saveBridge
  Twitterroutes.addRoutes(app)

  return

# update or create usermap
saveUsermap = (pumpid,esnid,esntoken, extra, callback) ->
  saveMap = new Usermap(
    id : Usermap.key(pumpid,esnid)
    user_pumpio : pumpid
    user_ESN : esnid
    oauth_token : esntoken
    extra_token : extra
    created : Date.now()
  )
  saveMap.save (cb) ->
   #console.log 'saved.'
  callback null, saveMap
  return

# update or create usermap
saveUsermapSelect = (pumpid, esnid, esntoken, extra, fromesn, toesn, callback) ->
  saveMap = new Usermap(
    id : Usermap.key(pumpid,esnid)
    user_pumpio : pumpid
    user_ESN : esnid
    oauth_token : esntoken
    extra_token : extra
    fromesn : fromesn
    toesn: toesn
    created : Date.now()
  )
  saveMap.save (cb) ->
   #console.log 'saved.'
  callback null, saveMap
  return


# only create and not update usermap
createUsermap = (pumpid,esnid,esntoken) ->
  async.waterfall [(callback) ->
    Usermap.create
      user_pumpio: pumpid
      user_ESN: esnid
      oauth_token: esntoken
    , callback
  ], (err, result) ->
  if err instanceof databank.AlreadyExistsError
    console.log err
    return
  if err
    console.log err
    return
  else
    return

# update or create a user
updateUserDB = (id, name, displayName, profileLink, profilePicLink) ->
  async.waterfall [(callback) ->
    saveUser = new User()
    saveUser.id = id
    saveUser.name = name
    saveUser.displayName = displayName
    saveUser.profileLink = profileLink
    saveUser.profilePicLink = profilePicLink
    saveUser.created = Date.now()
    saveUser.save callback
  ], (err, result) ->
  return

deleteUsermap = (id, callback) ->
  bank = Usermap.bank()
  bank.del Usermap.type, id, callback
  return

exports.saveUsermap = saveUsermap
exports.updateUserDB = updateUserDB
exports.addRoutes = addRoutes
