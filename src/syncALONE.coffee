# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

_ = require("underscore")
async = require("async")
Facebook = require("./facebook")
Usermap = require("./usermap")
Google = require("./google")
Config = require ("./config")
config = Config.config
Twitter = require("./twitter")(config)

databank = require("databank")
Databank = databank.Databank
DatabankObject = databank.DatabankObject

db = Databank.get(config.driver, config.params)
DatabankObject.bank = db

syncFromESN = () ->
  console.log 'syncFromESN'

  ####
  # Facebook
  try
    Usermap.scan ((user) ->
      if user.id.indexOf('@facebook') isnt -1
        console.log "start sync for facebook user"
        Facebook.sync(user)
    ), (err) ->
  catch err
    console.log 'Facebook Error!' + err

  # Twitter
  try
    Usermap.scan ((user) ->
      if user.id.indexOf('@twitter') isnt -1
        # do if true or undefined
        unless user.toesn is false
          console.log "start sync TOESN for twitter user"
          Twitter.syncToESN(user)
        unless user.fromesn is false
          console.log "start sync FROMESN for twitter user"
          Twitter.syncFromESN(user)
          Twitter.postLike(user)
    ), (err) ->
  catch err
    console.log 'Twitter Error!' + err

  # Google
  try
    Usermap.scan ((user) ->
      if user.id.indexOf('@google') isnt -1
        console.log "start sync for google user"
        Google.sync(user)
    ), (err) ->
  catch err
    console.log 'Google Error!' + err
  ####

  return

sync = () ->

  # Do this every xx minutes
  console.log '\n\n\n' + "starting sync deamon"
  interval = config.interval
  if not (interval?)
    interval =  15 * 60 * 1000 # 900 000 ms (15min)

  async.waterfall [
    (callback) ->
       db.connect(config.params, callback)
  ], (err, result) ->
    setInterval syncFromESN, interval
    syncFromESN()
  return

sync()
