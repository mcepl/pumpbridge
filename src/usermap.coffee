# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

_ = require("underscore")
async = require("async")
DatabankObject = require("databank").DatabankObject
Usermap = DatabankObject.subClass("usermap")

Usermap.schema =
  pkey: "id"
  fields: [
    "user_pumpio"
    "user_ESN"
    "oauth_token"
    "extra_token"
    "fromesn"
    "toesn"
    "created"
  ]
  indices: [
    "id"
    "user_pumpio"
    "user_ESN"
  ]

Usermap.beforeCreate = (props, callback) ->
  if not props.user_pumpio or not props.user_ESN
    callback new Error("Need 'user_pumpio' and 'user_ESN' in Usermap"), null
    return
  props.id = Usermap.key(props.user_pumpio, props.user_ESN)
  props.created = Date.now()
  callback null, props
  return

Usermap.key = (user_pumpio, user_ESN) ->
  user_pumpio + "_to_" + user_ESN

module.exports = Usermap
