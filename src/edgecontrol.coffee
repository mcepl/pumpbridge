# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

async = require("async")
_ = require("underscore")
Edge = require("./edge")


removeEdges = (userid, network, callback) ->
  delEdge = (edge, callback) ->
    unless edge.to.indexOf(network) is -1
      edge.del callback
    return

  async.parallel [
    (callback) ->
      return Edge.search(
        from: userid
      , (err, edges) ->
        if err
          callback err
        else
          async.each edges, delEdge, callback
        return
      )
    (callback) ->
      return Edge.search(
        to: userid
      , (err, edges) ->
        if err
          callback err
        else
          async.each edges, delEdge, callback
        return
      )
  ], callback
  return

addEdge = (from, to) ->
  async.waterfall [(callback) ->
   Edge.create
     from: from
     to: to
    , callback
    return

  ], (err, result) ->
    return

deleteEdge = (from, to) ->
  async.waterfall [(callback) ->
   bank = Edge.bank()
   bank.del Edge.type, Edge.key(from, to), callback
   return

  ], (err, result) ->
    return

exports.removeEdges = removeEdges
exports.deleteEdge = deleteEdge
exports.addEdge = addEdge
