# Most of the routes in the application
#
# Copyright 2013, E14N (https://e14n.com/)
# all changes Copyright 2014, Intevation GmbH (https://intevation.org)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

async = require("async")
_ = require("underscore")
PumpIOClientApp = require("pump.io-client-app")
RequestToken = PumpIOClientApp.RequestToken
userAuth = PumpIOClientApp.userAuth
userOptional = PumpIOClientApp.userOptional
userRequired = PumpIOClientApp.userRequired
noUser = PumpIOClientApp.noUser
Routes = require("./routes")
Config = require("./config")
config = Config.config

addRoutes = (app) ->

  Twitter = require("./twitter")(config)

  addAccount = (req, res, next) ->
    Twitter.getRequestToken req.site, (err, rt) ->
      if err
        next err
      else
        res.redirect Twitter.authorizeURL(rt)
      return

    return

  authorizedForTwitter = (req, res, next) ->
    hostname = "twitter.com"
    token = req.query.oauth_token
    verifier = req.query.oauth_verifier
    problem = req.query.oauth_problem
    user = req.user
    rt = undefined
    fuser = undefined
    access_token = undefined
    token_secret = undefined
    id = undefined
    object = undefined
    newUser = false
    unless token
      next new Error("No token returned.")
      return
    async.waterfall [
      (callback) ->
        RequestToken.get RequestToken.key(hostname, token), callback
      (results, callback) ->
        rt = results
        Twitter.getAccessToken req.site, rt, verifier, callback
      (token, secret, extra, callback) ->
        access_token = token
        token_secret = secret
        async.parallel [
          (callback) ->
            rt.del callback
          (callback) ->
            Twitter.whoami req.site, access_token, token_secret, callback
        ], callback
      (results, callback) ->
        object = results[1]
        object = JSON.stringify(object)
        object = JSON.parse(object)
        res.clearCookie('twitteruser')
        res.clearCookie('twitterid')
        res.cookie('twitterid',object.id, { maxAge: 900000, httpOnly: false })
        res.cookie('twitteruser',object.screen_name, { maxAge: 900000, httpOnly: false })
        Routes.saveUsermap(user.id ,object.id + "@twitter" ,access_token, token_secret, callback)
    ], (err) ->
      if err
        next err
      else
        res.redirect "/"
      return

    return

  # Routes
  console.log "Initializing Twitter routes"
  app.get "/add-account", userAuth, userRequired, addAccount
  app.get "/authorized-for-twitter", userAuth, userRequired, authorizedForTwitter
  return

exports.addRoutes = addRoutes
