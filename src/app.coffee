# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

async = require 'async'
databank = require 'databank'
Databank = databank.Databank
DatabankObject = databank.DatabankObject
PumpIOClientApp = require 'pump.io-client-app'
Sync = require('./sync')
Config = require('./config')
routes = require('./routes')
config = Config.config

async.waterfall [
  (callback) ->
    # sets up the config
    app = new PumpIOClientApp(config)

    routes.addRoutes app

    # Start the app
    app.log.debug
      port: config.port
      address: config.address
    , 'Starting app listener'

    app.run (err) ->
      if err
        app.log.error err
      else
        console.log 'Express server is listening
        on address %s port %d', config.address, config.port

        # Start Deamon!
        # or comment to the next hashtag after ')' and use
        # Sync.sync()
        # or start coffee syncALONE.coffee in a different window
        #exec = require("child_process").exec
        #child = undefined
        #child = exec("coffee syncALONE.coffee", (error, stdout, stderr) ->
        #  console.log "stdout: " + stdout
        #  console.log "stderr: " + stderr
        #  console.log "exec error: " + error  if error isnt null
        #  return
        #)
        #

      return
], (err, result) ->
