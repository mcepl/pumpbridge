# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

_ = require("underscore")
async = require("async")
Facebook = require("./facebook")
Usermap = require("./usermap")
Google = require("./google")
Config = require ("./config")
config = Config.config
Twitter = require("./twitter")(config)

syncFromESN = () ->
  console.log 'syncFromESN'

  # STEP1: get all from usermap *network (3) "usermap:mgebbe@io.intevation.de_to_100002056487693@facebook"
  # STEP2: for each user in network do sync
   # STEP 2.1 (do the network sync)
    # get all new POSTS from ESN
    # post all new POSTS to ESN
    # check all FROMESN and update them
    # check all toESN and update them

  ####
  # Facebook
  try
    Usermap.scan ((user) ->
      if user.id.indexOf('@facebook') isnt -1
        console.log "start sync for facebook user"
        Facebook.sync(user)
    ), (err) ->
  catch err
    console.log 'Error!' + err

  # Twitter
  try
    Usermap.scan ((user) ->
      if user.id.indexOf('@twitter') isnt -1
        console.log "start sync for twitter user"
        Twitter.sync(user)
    ), (err) ->
  catch err
    console.log 'Error!' + err


  # Google
  try
    Usermap.scan ((user) ->
      if user.id.indexOf('@google') isnt -1
        console.log "start sync for google user"
        Google.sync(user)
    ), (err) ->
  catch err
    console.log 'Error!' + err
  ####
  return


postParser = (post, user, network, callback) ->
  parsed = ""
  #console.log "\n" + post.id + "\n" + user.id + user.displayName
  #PROFILE_LINK PROFILE_NAME PROFILE_PIC_LINK_80x80=$3 POST_LINK POST_TIME CONTENT
  #text='<img src='$PROFILE_PIC_LINK_80x80'></img> <a href='$PROFILE_LINK'>'$PROFILE_NAME'</a> <a href='$POST_LINK'>writes</a> at '$POST_TIME':<br><br>'$CONTENT''

  if network is 'facebook'
    if user? and user.profilePicLink?
      profilePicLink = user.profilePicLink
      #.replace("50x50", "80x80")
    else
      profilePicLink = 'http://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/F_icon.svg/80px-F_icon.svg.png'

    if post.type is 'status'
      parsed = "<img src='" + profilePicLink + "'></img> <a href='" + user.profileLink + "'>" + user.displayName + "</a> <a href='https://www.facebook.com/" + post.id + "'> writes</a> via " + network + " at " + post.updated_time + ":<br><br>" + post.message

    if post.type is 'photo'
      parsed = "<img src='" + profilePicLink + "'></img> <a href='" + user.profileLink + "'>" + user.displayName + "</a> <a href='"+ post.link + "'> writes</a> via " + network + " at " + post.updated_time + ":<br>"
      parsed += "<br>" + post.message if post.message? and post.message isnt ""
      parsed += "<br><img src='" + post.picture + "'></img>"

    if post.type is 'link' or post.type is 'video'
      parsed = "<img src='" + profilePicLink + "'></img> <a href='" + user.profileLink + "'>" + user.displayName + "</a> <a href='https://www.facebook.com/" + post.id + "'> writes</a> via " + network + " at " + post.updated_time + ":<br>"
      parsed += "<br>" + post.message if post.message? and post.message isnt ""
      parsed += "<br>" + post.description if post.description?
      parsed +=  "<br><a href='" + post.link + "'>"+ post.link + "</a>" if post.link?
      parsed += "<br><img src='" + post.picture + "'></img>" if post.picture?

    callback null, parsed

  if network is 'google'
    parsed = "<img src='" + post.actor.image.url + "'></img> <a href='" + post.actor.url + "'>" + post.actor.displayName + "</a> <a href='"+ post.object.url + "'> writes</a> via " + network + " at " + post.updated + ":<br><br>" + post.object.content
    if not post.object.attachments?
      _.each post.object.attachments, (attachment) ->

        if attachment.objectType == "photo"
          parsed += "<br><img src='" + attachment.image.url + "'></img>"

        if attachment.objectType == "article"
          parsed +=  "<br><a href='" + attachment.url + "'>"+ attachment.displayName + "</a>"
          if not attachment.image?
            parsed += "<br><img src='" + attachment.image.url + "'></img>"

        if attachment.objectType == "video"
          parsed +=  "<br><a href='" + attachment.url + "'>"+ attachment.displayName + "</a>"
          if not attachment.image?
            parsed += "<br><img src='" + attachment.image.url + "'></img>"

    callback null, parsed

  if network is 'twitter'

    post.text = post.text.replace(/(http[s]?:\/\/[^ ]+)/ig,'<a href="$1">$1<\/a>')

    parsed = "<img src='" + post.user.profile_image_url + "'></img> <a href='https://twitter.com/account/redirect_by_id/" + post.user.id + "'>" + post.user.name + "</a> <a href='https://twitter.com/" + post.user.screen_name + "/status/" + post.id_str + "'> writes </a> via " + network + " at " + post.created_at + ":<br><br>" + post.text
    _.each post.entities.media, (attachment) ->
      parsed += "<br><img src='" + attachment.media_url + "'></img>"
    callback null, parsed

  return


sync = () ->

  # Do this every xx minutes
  console.log '\n\n\n' + "starting sync deamon"
  interval = config.interval
  if not (interval?)
    interval =  15 * 60 * 1000 # 900 000 ms (15min)
  setInterval syncFromESN, interval

  syncFromESN()

  return

exports.postParser = postParser
exports.sync = sync
