#!/bin/bash

cd /home/pumpbridge/pumpbridge/src

until coffee syncALONE.coffee; do
    echo "sync crashed with exit code $?. Respawning.." >&2
    sleep 1
done
