# twitter.js
#
# data object representing twitter.com
#
# Copyright 2013, E14N (https://e14n.com/)
# all changes Copyright 2014, Intevation GmbH (https://intevation.org)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

_ = require("underscore")
async = require("async")
OAuth = require("oauth").OAuth
PumpIOClientApp = require("pump.io-client-app")
DatabankObject = require("databank").DatabankObject
RequestToken = PumpIOClientApp.RequestToken
Usermap = require("./usermap")
Pump = require("./pumpio")
FromESN = require("./fromESN")
ToESN = require("./toESN")
Sync = require("./sync")
util = require("util")
htmlToText = require('html-to-text')
twitter = require("twitter")

module.exports = (config) ->
  bridgeid = config.bridgeid
  client_id = config.twclient_id
  client_secret = config.twclient_secret
  interval = config.interval
  if not (interval?)
    interval =  15 * 60 * 1000
  request_token_endpoint = "https://api.twitter.com/oauth/request_token"
  access_token_endpoint = "https://api.twitter.com/oauth/access_token"
  authorization_endpoint = "https://api.twitter.com/oauth/authorize"
  whoami_endpoint = "https://api.twitter.com/1.1/account/verify_credentials.json"
  hostname = "twitter.com"

  Twitter =
    getRequestToken: (site, callback) ->
      oa = Twitter.getOAuth(site)
      async.waterfall [
        (callback) ->
          oa.getOAuthRequestToken callback
        (token, secret, other, callback) ->
          RequestToken.create
            token: token
            secret: secret
            hostname: hostname
          , callback
      ], callback
      return

    authorizeURL: (rt) ->
      separator = undefined
      if _.contains(authorization_endpoint, "?")
        separator = "&"
      else
        separator = "?"
      authorization_endpoint + separator + "oauth_token=" + rt.token

    getAccessToken: (site, rt, verifier, callback) ->
      oa = Twitter.getOAuth(site)
      oa.getOAuthAccessToken rt.token, rt.secret, verifier, callback
      return

    whoami: (site, token, secret, callback) ->
      oa = Twitter.getOAuth(site)
      async.waterfall [(callback) ->
        oa.get whoami_endpoint, token, secret, callback
        return
      ], (err, doc, response) ->
        obj = undefined
        if err
          callback err, null
        else
          try
            obj = JSON.parse(doc)
            callback null, obj
          catch e
            callback e, null
        return

      return

    syncToESN: (user) ->
      me = user.user_pumpio
      id = user.user_ESN.substr(0,user.user_ESN.indexOf('@'))
      token = user.oauth_token
      secret = user.extra_token
      twit = new twitter(
        consumer_key: client_id
        consumer_secret: client_secret
        access_token_key: token
        access_token_secret: secret
      )

      # GET PUBLIC PUMP POSTS AND POST THEM
      async.waterfall [
        (callback) ->
          Usermap.search {id: me + '_to_' + me}, callback
        (user, callback) ->
          if (user[0]?)
            console.log "get feed from" + me
            Pump.getUserFeed(user[0],callback)
          else
            return
        (feed, callback) ->
          return if not feed?
          ti = new Date().getTime() - interval
          async.eachLimit feed.items, 1, ((post, callback) ->
            # do for each post
            ts = Date.parse(post.updated)
            if (ts >= ti and post.verb is "post" or post.verb is "share") and (post.object.objectType is "note" or post.object.objectType is "image") and (Pump.isPublicActivity(post)) and (typeof post.object.deleted is "undefined")
              ToESN.search {uid: post.object.id + "@twitter"}, (err, result) ->
                if result.length is 0
                  # post to twitter
                  status = ""
                  text = post.object.content
                  text = htmlToText.fromString(text,wordwrap: 140)
                  #text = post.object.content.replace(/<(?:.|\n)*?>/gm, '')
                  #re = new RegExp(String.fromCharCode(160), "g")
                  #text = text.replace(re, " ")
                  if post.verb is "share"
                    status = post.object.author.url + " writes: "
                  if text.length <= 140
                    status += text
                  else
                    status += text.substr(0, 140 - (post.object.url.length + 2)) + " " + post.object.url
                  twit.verifyCredentials((data) ->
                    #console.log util.inspect(data)
                    return
                  ).updateStatus status, (data) ->
                    async.waterfall [
                      (callback) ->
                        savePost = new ToESN()
                        savePost.uid = post.object.id + "@twitter"
                        savePost.sourceUser = post.actor.id
                        savePost.sourcePost = post.object.id
                        savePost.targetUser = user.user_ESN
                        savePost.targetPost = data.id
                        savePost.recipientUser = 'public'
                        savePost.updated = Date.now()
                        savePost.save callback
                    ], (err, result) ->
                    #console.log util.inspect(data)
                    return
            callback null, 'done'
          ), (err) ->
            callback null, 'done'
      ],(err, result) ->
        console.log "done ToESN twitter user " + me
        return
      return

    syncFromESN: (user) ->
      me = user.user_pumpio
      id = user.user_ESN.substr(0,user.user_ESN.indexOf('@'))
      token = user.oauth_token
      secret = user.extra_token
      twit = new twitter(
        consumer_key: client_id
        consumer_secret: client_secret
        access_token_key: token
        access_token_secret: secret
      )
      ######################################
      # get tweets and post them to pump.io#
      ######################################
      twit.verifyCredentials((data) ->
        #console.log util.inspect(data)
        console.log "get twitter HomeTimeline " + me
        return
      ).getHomeTimeline {include_rts: true, count: 15 }, (data) ->
        console.log "got twitter HomeTimeline " + me
        return if not data? or data.length is 0
        console.log "data isnt empty for " + me
        ti = (new Date().getTime() - interval)
        async.eachLimit data, 3, ((tweet, cb) ->
          async.waterfall [
            (callback) ->
              FromESN.search {uid: tweet.id_str + "@twitter_to_" + me}, callback
            (result, callback) ->
              return if result.length isnt 0 or tweet.user.id_str is id
              console.log "tweet found"
              twitterdate = new Date(Date.parse(tweet.created_at.replace(/( +)/, " UTC$1")))
              ts = Date.parse(twitterdate)
              if (ts >= ti)
                Sync.postParser tweet, null, 'twitter', callback
              else
                return
            (parsed, callback) ->
              console.log "try to post new tweet"
              Pump.postUser bridgeid, me, parsed, callback
            (pumppost, callback) ->
              try
                pumppost = JSON.parse(pumppost)
              catch err
                console.log "parse err " + err
                return
              FromESN.create
                postid: tweet.id_str + "@twitter"
                sourceUser: tweet.user.id
                sourcePost: 'https://twitter.com/'+tweet.user.screen_name+"/status/" + tweet.id_str
                pumpPost: pumppost.object.id
                recipientUser: me
                created: Date.now()
              , callback
          ], (err, result) ->
            console.log "post a tweet to user " + me
            cb
        ), (err) ->
          if (err)
            console.log "Error: " + err
          else
            console.log "done FromESN twitter user " + me
      return

    postLike: (user) ->
      i = 0
      me = user.user_pumpio
      id = user.user_ESN.substr(0,user.user_ESN.indexOf('@'))
      token = user.oauth_token
      secret = user.extra_token
      twit = new twitter(
        consumer_key: client_id
        consumer_secret: client_secret
        access_token_key: token
        access_token_secret: secret
      )
      console.log "try 2 find twitter likes for " + me
      async.waterfall [
        (callback) ->
          Usermap.search {id: me + '_to_' + me}, callback
        (pumpuser, callback) ->
          Pump.getLikes(pumpuser[0], callback)
        (likearray, callback) ->
          return if not (likearray?) or likearray.length is 0
          likes = JSON.parse(likearray)
          return if not (likes?) or likes.items.length is 0
          async.each likes.items,((note, cb) ->
            async.waterfall [
              (callback) ->
                 FromESN.search {recipientUser: me, pumpPost: note.id}, callback
              (fromesn, callback) ->
                if (fromesn?) and fromesn.length isnt 0 and fromesn[0].sourcePost.indexOf('twitter.com') isnt -1
                  #console.log fromesn[0]
                  obj = fromesn[0].postid.substr(0,fromesn[0].postid.indexOf('@'))
                  twit.post "/favorites/create.json",
                    id: obj
                  , (data) ->
                    console.log "Twitter " + obj + " liked"
            ], (err, result) ->
          ), (err) ->
      ], (err, result) ->
      return

    getOAuth: (site) ->
      new OAuth(request_token_endpoint, access_token_endpoint, client_id, client_secret, "1.0", site.url("/authorized-for-twitter"), "HMAC-SHA1", null, # nonce size; use default
        "User-Agent": site.userAgent()
      )

  Twitter
