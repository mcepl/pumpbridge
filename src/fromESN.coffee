# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

_ = require("underscore")
async = require("async")
DatabankObject = require("databank").DatabankObject
FromESN = DatabankObject.subClass("fromESN")

FromESN.schema =
  pkey: "uid"
  fields: [
    "postid"
    "sourceUser"
    "sourcePost"
    "pumpPost"
    "recipientUser"
    "created"
    "updated"
  ]
  indices: [
    "uid"
    "postid"
    "sourceUser"
    "sourcePost"
    "pumpPost"
    "recipientUser"
  ]

FromESN.beforeCreate = (props, callback) ->
  if not props.sourcePost or not props.sourceUser
    callback new Error("Need 'sourceUser' and 'sourceUser'"), null
    return
  props.uid = FromESN.key(props.postid , props.recipientUser)
  props.created = Date.now()
  props.updated = Date.now()
  callback null, props
  return

FromESN::beforeSave = (callback) ->
  props = this
  if not props.sourcePost or not props.sourceUser
    callback new Error("Need 'sourceUser' and 'sourceUser'"), null
    return
  props.updated = Date.now()
  callback null
  return

FromESN.key = (postid, recipientUser) ->
  postid + "_to_" + recipientUser

module.exports = FromESN
