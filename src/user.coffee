# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

_ = require("underscore")
async = require("async")
DatabankObject = require("databank").DatabankObject
User = DatabankObject.subClass("bridgeuser")

User.schema =
  pkey: "id"
  fields: [
    "name"
    "displayName"
    "profileLink"
    "profilePicLink"
    "created"
  ]
  indices: [
    "name"
    "displayName"
  ]

User.beforeCreate = (props, callback) ->
  if not props.id
    callback new Error("Need 'id' in User"), null
    return
  props.created = Date.now()
  callback null, props
  return

module.exports = User
