# Copyright (C) 2014 by Intevation GmbH
# Author: Mathias Gebbe <mgebbe@intevation.de>
#
# This file is Free Software under the Apache License, Version 2.0;
# and comes with NO WARRANTY!
# See the documentation coming with pumpbridge for details.

https = require("https")
async = require("async")
_ = require("underscore")
Routes = require("./routes")
querystring = require("querystring")
EdgeControl = require("./edgecontrol")
Edge = require("./edge")
Host = require("../node_modules/pump.io-client-app/lib/models/host")
OAuth = require('oauth')
Usermap = require("./usermap")


#######################################
##### get user info pumpio        #####
#######################################
getUser = (user) ->
  id = user.user_ESN.substr(0,user.user_ESN.indexOf('@'))
  server = user.user_ESN.substr(user.user_ESN.indexOf('@')+1,user.user_ESN.length)
  token = user.oauth_token.substr(0,user.oauth_token.indexOf(';'))
  secret = user.oauth_token.substr(user.oauth_token.indexOf(';')+1,user.oauth_token.length)
  # get tokens and ask pump.io api for userinformation then update userdb
  async.waterfall [
   (callback) ->
     Host.search {hostname: server}, callback
   (host,callback)  ->
     return if not (host?)
     # get host from db
     host = JSON.stringify(host)
     host = JSON.parse(host)
     return if not (host[0]?)
     oauth = new OAuth.OAuth(host[0].request_token_endpoint, host[0].access_token_endpoint, host[0].client_id, host[0].client_secret, "1.0A", null, "HMAC-SHA1")
     oauth.get "https://" + server + "/api/user/"+id, token, secret, callback
  ], (err,result) ->
    #console.log 'PUMP BODY:' + result
    try
      result = JSON.parse(result)
      id = result.profile.id.substr(result.profile.id.indexOf(':')+1,result.profile.id.length)
      Routes.updateUserDB(id,result.profile.preferredUsername,result.profile.displayName,result.profile.url,result.profile.image.url)
    catch err
      console.log 'pumpio getUser error' + err
  return

postUser = (bridgeid, to, text, callback) ->

  bridgename = bridgeid.substr(0,bridgeid.indexOf('@'))
  server = bridgeid.substr(bridgeid.indexOf('@')+1,bridgeid.length)
  token=undefined
  secret=undefined

  async.waterfall [
   (callback) ->
     Usermap.search {id: bridgeid+'_to_'+bridgeid}, callback
   (bridgeuser, callback) ->
     token = bridgeuser[0].oauth_token.substr(0,bridgeuser[0].oauth_token.indexOf(';'))
     secret = bridgeuser[0].oauth_token.substr(bridgeuser[0].oauth_token.indexOf(';')+1,bridgeuser[0].oauth_token.length)
     Host.search {hostname: server}, callback
   (host,callback)  ->
     # get host from db
     return if not (host?)
     host = JSON.stringify(host)
     host = JSON.parse(host)
     return if not (host[0]?)
     activity =
       verb: "post"
       #cc: [
       #  id: "http://activityschema.org/collection/public"
       #  objectType: "collection"
       #]
       to: [
         id: "acct:" + to
         objectType: "person"
       ]
       object:
         objectType: "note"
         content: text
     oauth = new OAuth.OAuth(host[0].request_token_endpoint, host[0].access_token_endpoint, host[0].client_id, host[0].client_secret, "1.0A", null, "HMAC-SHA1")
     oauth.post "https://" + server + "/api/user/" + bridgename + "/feed", token, secret, JSON.stringify(activity), 'application/json', callback
  ], (err,result) ->
    #console.log 'PUMP BODY:' + result
    callback null, result

  return

#######################################
##### get user pumpio feed        #####
#######################################
getUserFeed = (user, callback) ->
  id = user.user_ESN.substr(0,user.user_ESN.indexOf('@'))
  server = user.user_ESN.substr(user.user_ESN.indexOf('@')+1,user.user_ESN.length)
  token = user.oauth_token.substr(0,user.oauth_token.indexOf(';'))
  secret = user.oauth_token.substr(user.oauth_token.indexOf(';')+1,user.oauth_token.length)
  # get tokens and ask pump.io api for userinformation then update userdb
  async.waterfall [
   (callback) ->
     Host.search {hostname: server}, callback
   (host,callback)  ->
     # get host from db
     return if not (host?)
     host = JSON.stringify(host)
     host = JSON.parse(host)
     return if not (host[0]?)
     oauth = new OAuth.OAuth(host[0].request_token_endpoint, host[0].access_token_endpoint, host[0].client_id, host[0].client_secret, "1.0A", null, "HMAC-SHA1")
     oauth.get "https://" + server + "/api/user/"+id+"/feed", token, secret, callback
  ], (err,result) ->
    #console.log 'PUMP BODY:' + result
    try
      result = JSON.parse(result)
      callback null, result
    catch err
      console.log "pump.io Error"
      callback null, null
  return

#######################################
##### get user pumpio note        #####
#######################################
getNote = (user, noteurl, callback) ->
  id = user.user_ESN.substr(0,user.user_ESN.indexOf('@'))
  server = user.user_ESN.substr(user.user_ESN.indexOf('@')+1,user.user_ESN.length)
  token = user.oauth_token.substr(0,user.oauth_token.indexOf(';'))
  secret = user.oauth_token.substr(user.oauth_token.indexOf(';')+1,user.oauth_token.length)

  # get tokens and ask pump.io api for userinformation then update userdb
  async.waterfall [
   (callback) ->
     Host.search {hostname: server}, callback
   (host,callback)  ->
     return if not (host?)
     # get host from db
     host = JSON.stringify(host)
     host = JSON.parse(host)
     return if not (host[0]?)
     oauth = new OAuth.OAuth(host[0].request_token_endpoint, host[0].access_token_endpoint, host[0].client_id, host[0].client_secret, "1.0A", null, "HMAC-SHA1")
     oauth.get noteurl, token, secret, callback
  ], (err,result) ->
    #console.log 'PUMP BODY:' + result
    if err
      callback null, null
    else
      result = JSON.parse(result)
      callback null, result
  return

getLikes = (user, callback) ->
  return if not (user?)
  id = user.user_ESN.substr(0,user.user_ESN.indexOf('@'))
  server = user.user_ESN.substr(user.user_ESN.indexOf('@')+1,user.user_ESN.length)
  token = user.oauth_token.substr(0,user.oauth_token.indexOf(';'))
  secret = user.oauth_token.substr(user.oauth_token.indexOf(';')+1,user.oauth_token.length)
  # https://io.intevation.de/api/user/mgebbe/favorites
  url = "https://" + server + "/api/user/" + id + "/favorites"
  # get tokens and ask pump.io api for userinformation then update userdb
  async.waterfall [
   (callback) ->
     Host.search {hostname: server}, callback
   (host,callback)  ->
     # get host from db
     return if not (host?)
     host = JSON.stringify(host)
     host = JSON.parse(host)
     return if not (host[0]?)
     #console.log JSON.stringify host
     oauth = new OAuth.OAuth(host[0].request_token_endpoint, host[0].access_token_endpoint, host[0].client_id, host[0].client_secret, "1.0A", null, "HMAC-SHA1")
     oauth.get url, token, secret, callback
  ], (err,result) ->
    if err
      callback null, null
    else
      callback null, (result)
  return



isPublicActivity = (act) ->
  recip = []
  _.each [
    "to"
    "cc"
    "bto"
    "bcc"
  ], (prop) ->
    recip = recip.concat(act[prop])  if _.isArray(act[prop])
    return

  _.some recip, (rec) ->
    rec.objectType is "collection" and rec.id is "http://activityschema.org/collection/public"


exports.isPublicActivity = isPublicActivity
exports.getUserFeed = getUserFeed
exports.getUser = getUser
exports.getNote = getNote
exports.getLikes = getLikes
exports.postUser = postUser
